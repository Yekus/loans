﻿using System.Threading.Tasks;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Core.Repositories;
using Loans.Infrastructure.Services;
using Moq;
using Xunit;

namespace Loans.Tests.Services
{
    public class UserServiceTests
    {
        [Fact]
        public async Task register_should_create_user_once()
        {
            var userRepositoryMock = new Mock<IUserRepository>();
            var encrypterMock = new Mock<IEncrypter>();
            var mapperMock = new Mock<IMapper>();
            
            
            var userService = new UserService(userRepositoryMock.Object, encrypterMock.Object, mapperMock.Object);
            await userService.RegisterAsync("mati@gmail.com", "secret1", "Matt", "Kuchta", "12345", "AJ123");
            
            
            userRepositoryMock.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once);
        }
    }
}