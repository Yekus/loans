﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loans.Core.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public string PasswordSalt { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string Pesel { get; protected set; }
        public string DocumentNum { get; protected set; }

        protected User() { }

        private User(
            string email, 
            string password, 
            string passwordSalt, 
            string firstName, 
            string lastName, 
            string pesel, 
            string documentNum)
        {
            Id = Guid.NewGuid();
            Email = email.ToLowerInvariant();
            Password = password;
            PasswordSalt = passwordSalt;
            FirstName = firstName;
            LastName = lastName;
            Pesel = pesel;
            DocumentNum = documentNum;
            CreatedDt = DateTime.Now;
            UpdatedDt = DateTime.Now;
        }
        
        public static User Create(
            string email, 
            string password, 
            string passwordSalt, 
            string firstName, 
            string lastName, 
            string pesel, 
            string documentNum)
            => new User(email, password, passwordSalt, firstName, lastName, pesel, documentNum);

        public void SetPassword(string value)
        {
            Password = value;
        }

        public void SetPasswordSalt(string value)
        {
            PasswordSalt = value;
        }
    }
}