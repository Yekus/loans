﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;

namespace Loans.Core.Entities
{
    public class Offer : BaseEntity
    {
        [Required] public decimal MinimumValue { get; protected set; }
        [Required] public decimal MaximumValue { get; protected set; }
        [Required] public int Interest { get; protected set; }
        [Required] public int MinimumBikPoints { get; protected set; }
        public Guid BankId { get; protected set; }
        public Bank Bank { get; protected set; }

        public Offer(Guid bankId, decimal minimumValue, decimal maximumValue, int interest, int minimumBikPoints, Bank bank)
        {
            Id = Guid.NewGuid();
            BankId = bankId;
            MinimumValue = minimumValue;
            MaximumValue = maximumValue;
            Interest = interest;
            MinimumBikPoints = minimumBikPoints;
            Bank = bank;
        }
    }
}