﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loans.Core.Entities
{
    public abstract class Entity
    {
        [Required] protected DateTime CreatedDt { get; set; }
        [Required] protected DateTime UpdatedDt { get; set; }
    }
}