﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loans.Core.Entities
{
    public class Loan : BaseEntity
    {
        [Required] public decimal Value { get; protected set; }
        [Required] public User User { get; protected set; }
        [Required] public Bank Bank { get; protected set; }
        [Required] public Offer Offer { get; protected set; }

        public Loan(decimal value, User user, Bank bank, Offer offer)
        {
            Id = Guid.NewGuid();
            Value = value;
            User = user;
            Bank = bank;
            Offer = offer;
        }
    }
}