﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loans.Core.Entities
{
    public class Bank : BaseEntity
    {
        [Required] public string Name { get; private set; }
        [Required] public decimal TotalSum { get; private set; }

        protected Bank() {}

        private Bank(string name, decimal totalSum)
        {
            Id = Guid.NewGuid();
            Name = name;
            TotalSum = totalSum;
        }
        
        public static Bank Create(string name, decimal totalSum)
            => new Bank(name, totalSum);
    }
}