﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loans.Core.Entities
{
    public abstract class BaseEntity : Entity
    {
        [Key] 
        public Guid Id { get; set; }
    }
}