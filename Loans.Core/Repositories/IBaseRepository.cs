﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Loans.Core.Entities;

namespace Loans.Core.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : Entity
    {
        Task<TEntity> FindByIdAsync(Guid id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<IEnumerable<TEntity>> GetByAsync(Func<TEntity, bool> expression);
        Task AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}