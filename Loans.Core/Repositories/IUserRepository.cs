﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Loans.Core.Entities;

namespace Loans.Core.Repositories
{
    public interface IUserRepository : IBaseRepository<User>, IRepository
    {
        //IEnumerable<User> FullGetBy(Expression<Func<User, bool>> expression);
        Task<User> FindByEmailAsync(string email);
    }
}