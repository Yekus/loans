﻿using System.Threading.Tasks;
using Loans.Core.Entities;

namespace Loans.Core.Repositories
{
    public interface IBankRepository : IBaseRepository<Bank>, IRepository
    {
        Task<Bank> FindByNameAsync(string name);
        Task<decimal> GetTotalSumByNameAsync(string name);
    }
}