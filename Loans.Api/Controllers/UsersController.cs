﻿using System.Threading.Tasks;
using Loans.Infrastructure.Commands;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace Loans.Api.Controllers
{
    public class UsersController : ApiControllerBase
    {
        private readonly IUserService _userSvc;
        private readonly ICommandDispatcher _commandDispatcher;
        
        public UsersController(IUserService userSvc, 
            ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _userSvc = userSvc;
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet("{email}")]
        public async Task<IActionResult> Get(string email)
        {
            var user = await _userSvc.GetAsync(email);
            if (user == null)
            {
                return NotFound();
            }

            return Json(user);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userSvc.GetAllAsync();
            if (users == null)
            {
                return NotFound();
            }
            return Json(users);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateUser command)
        {
            await CommandDispatcher.DispatchAsync(command);

            return Created($"users/{command.Email}", new object());
        }
        
        [HttpPut]
        [Route("password")]
        public async Task<IActionResult> Put([FromBody]ChangeUserPassword command)
        {
            await CommandDispatcher.DispatchAsync(command);

            return NoContent();
        }
        
        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Put([FromBody]UpdateUserData command)
        {
            await CommandDispatcher.DispatchAsync(command);

            return NoContent();
        }
    }
}