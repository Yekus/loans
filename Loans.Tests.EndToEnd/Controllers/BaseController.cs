﻿using System.Net.Http;
using System.Text;
using Loans.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;

namespace Loans.Tests.EndToEnd.Controllers
{
    public abstract class BaseController
    {
        protected readonly HttpClient Client;
        
        public BaseController()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            Client = server.CreateClient();
        }
        
        protected static StringContent GetSerialized(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}