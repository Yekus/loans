﻿using System.Net;
using System.Threading.Tasks;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.DTO;
using Newtonsoft.Json;
using Xunit;

namespace Loans.Tests.EndToEnd.Controllers
{
    public class UsersControllerTests : BaseController
    {
        [Fact]
        public async Task email_should_exist()
        {
            var email = "mattyekus@gmail.com";
            var response = await Client.GetAsync($"users/{email}");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<UserDto>(responseString);

            Assert.Equal(email, user.Email);
        }

        [Fact]
        public async Task email_should_not_exist()
        {
            var email = "zmyslonymail1@gmail.com";
            var response = await Client.GetAsync($"users/{email}");
            
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task unique_email_should_be_created()
        {
            var command = new CreateUser
            {
                Email = "nowymail@gmail.com",
                DocumentNum = "QWE1234",
                FirstName = "Piotr",
                LastName = "Kowal",
                Password = "admin",
                Pesel = "123456782"
            };
            
            
            var response = await Client.PostAsync("users", GetSerialized(command));
            
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal($"users/{command.Email}",response.Headers.Location.ToString());
        }

        [Fact]
        public async Task given_valid_password_should_be_updated()
        {
            var command = new ChangeUserPassword
            {
                CurrentPassword = "secret",
                NewPassword = "nosecret"
            };
            
            var response = await Client.PutAsync("users/password", GetSerialized(command));
            
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
        
        [Fact]
        public async Task given_existing_user_data_user_should_be_updated()
        {
            var command = new UpdateUserData()
            {
                Email = "mattyekus@gmail.com",
                DocumentNum = "POL123",
                FirstName = "InneImie",
                LastName = "Nazwisko",
                Pesel = "1234567"
            };
            
            var response = await Client.PutAsync("users/update", GetSerialized(command));
            
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}