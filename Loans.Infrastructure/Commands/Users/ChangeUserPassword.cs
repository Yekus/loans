﻿namespace Loans.Infrastructure.Commands.Users
{
    public class ChangeUserPassword : ICommand
    {
        public string Email { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}