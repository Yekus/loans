﻿namespace Loans.Infrastructure.Commands.Users
{
    public class UpdateUserData : ICommand
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pesel { get; set; }
        public string DocumentNum { get; set; }
    }
}