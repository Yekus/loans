﻿using System.Runtime.InteropServices.ComTypes;

namespace Loans.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool IsEmpty(this string value)
            => value.Length == 0;
    }
}