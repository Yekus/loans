﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Core.Repositories;
using Loans.Infrastructure.DTO;

namespace Loans.Infrastructure.Services
{
    public class BankService : IBankService
    {
        private readonly IBankRepository _bankRepository;
        private readonly IMapper _mapper;
        
        public BankService(IBankRepository bankRepository, IMapper mapper)
        {
            _bankRepository = bankRepository;
            _mapper = mapper;
        }
        
        public async Task AddBankAsync(string name, decimal totalSum)
        {
            var bank = await _bankRepository.FindByNameAsync(name);
            if (bank != null)
            {
                throw new Exception($"Bank with name: '{name}' already exist");
            }

            bank = Bank.Create(name, totalSum);
            await _bankRepository.AddAsync(bank);
        }

        public async Task UpdateBankAsync(Guid id, string name, decimal totalSum)
        {
            await Task.CompletedTask;
        }

        public async Task<BankDto> GetAsync(string name)
        {
            var bank = await _bankRepository.FindByNameAsync(name);
            return _mapper.Map<Bank, BankDto>(bank);
        }
    }
}