﻿using System;
using System.Threading.Tasks;
using Loans.Core.Entities;
using Loans.Infrastructure.DTO;

namespace Loans.Infrastructure.Services
{
    public interface IBankService : IService
    {
        Task AddBankAsync(string name, decimal totalSum);
        Task UpdateBankAsync(Guid id, string name, decimal totalSum);
        Task<BankDto> GetAsync(string name);
    }
}