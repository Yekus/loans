﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Loans.Core.Entities;
using Loans.Infrastructure.DTO;

namespace Loans.Infrastructure.Services
{
    public interface IUserService : IService
    {
        Task RegisterAsync(string email, string password, string firstName, string lastName, string pesel, string documentNum);
        Task LoginAsync(string email, string password);
        Task<UserDto> GetAsync(string email);
        Task<IEnumerable<UserDto>> GetAllAsync();
        Task UpdateAsync(User user);
    }
}