﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Core.Repositories;
using Loans.Infrastructure.DTO;

namespace Loans.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncrypter _encrypter;
        private readonly IMapper _mapper;
        
        public UserService(IUserRepository userRepository, IEncrypter encrypter, IMapper mapper)
        {
            _userRepository = userRepository;
            _encrypter = encrypter;
            _mapper = mapper;
        }

        public async Task RegisterAsync(string email, string password, string firstname, string lastname, string pesel, string documentnum)
        {
            var user = await _userRepository.FindByEmailAsync(email);
            if (user != null)
            {
                throw new Exception($"User with email: '{email}' already exists.");
            }
            
            
            var salt = _encrypter.GetSalt(password);
            var hash = _encrypter.GetHash(password, salt);
            user = User.Create(email, hash, salt, firstname, lastname, pesel, documentnum);
            await _userRepository.AddAsync(user);
        }

        public async Task LoginAsync(string email, string password)
        {
            var user = await _userRepository.FindByEmailAsync(email);
            if (user != null)
            {
                var salt = _encrypter.GetSalt(password);
                var hash = _encrypter.GetHash(password, salt);

                if (user.Password == hash)
                {
                    return;
                }
            }
            throw new Exception("Invalid credentials.");
        }

        public async Task<UserDto> GetAsync(string email)
        {
            var user = await _userRepository.FindByEmailAsync(email);
            return _mapper.Map<User, UserDto>(user);
        }

        public async Task<IEnumerable<UserDto>> GetAllAsync()
        {
            var users = await _userRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(users);
        }

        public async Task UpdateAsync(User user)
        {
            var userBefore = await _userRepository.FindByEmailAsync(user.Email);
            
            user.Id = user.Id == Guid.Empty ? userBefore.Id : user.Id;
            if(user.Password == null) user.SetPassword(userBefore.Password);
            if(user.PasswordSalt == null) user.SetPasswordSalt(userBefore.PasswordSalt);
            
            await _userRepository.UpdateAsync(user);
        }
    }
}