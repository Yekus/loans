﻿using System.Reflection;
using Autofac;
using Loans.Infrastructure.Commands;
using Loans.Infrastructure.Extensions;
using Loans.Infrastructure.Settings;
using Microsoft.Extensions.Configuration;

namespace Loans.Infrastructure.IoC.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
                .SingleInstance();
        }
    }
}