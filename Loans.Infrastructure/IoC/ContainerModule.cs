﻿using System;
using Autofac;
using Loans.Infrastructure.Extensions;
using Loans.Infrastructure.IoC.Modules;
using Loans.Infrastructure.Mappers;
using Loans.Infrastructure.Settings;
using Microsoft.Extensions.Configuration;

namespace Loans.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize())
                .SingleInstance();
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule(new SettingsModule(_configuration));
        }
    }
}