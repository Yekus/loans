﻿using System;

namespace Loans.Infrastructure.DTO
{
    public class UserDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pesel { get; set; }
        public string DocumentNum { get; set; }
        public DateTime CreatedDt { get; set; }
    }
}