﻿namespace Loans.Infrastructure.DTO
{
    public class BankDto
    {
        public string Name { get; set; }
        public decimal TotalSum { get; set; }
    }
}