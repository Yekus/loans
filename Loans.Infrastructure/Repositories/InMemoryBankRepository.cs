﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Loans.Core.Entities;
using Loans.Core.Repositories;

namespace Loans.Infrastructure.Repositories
{
    public class InMemoryBankRepository : IBankRepository
    {
        private static ISet<Bank> _banks = new HashSet<Bank>();

        public async Task<Bank> FindByIdAsync(Guid id)
            => await Task.FromResult(_banks.SingleOrDefault(x => x.Id.Equals(id)));

        public async Task<IEnumerable<Bank>> GetAllAsync()
            => await Task.FromResult(_banks);

        public async Task<IEnumerable<Bank>> GetByAsync(Func<Bank, bool> expression)
            => await Task.FromResult(_banks.Where(expression));

        public async Task AddAsync(Bank entity)
        {
            _banks.Add(entity);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Bank entity)
        {
            await Task.CompletedTask;
        }

        public async Task DeleteAsync(Bank entity)
        {
            var bank = await FindByIdAsync(entity.Id);
            if (bank != null)
            {
                _banks.Remove(entity);
            }

            await Task.CompletedTask;
        }

        public async Task<Bank> FindByNameAsync(string name)
            => await Task.FromResult(_banks.SingleOrDefault(x => x.Name.Equals(name)));

        public async Task<decimal> GetTotalSumByNameAsync(string name)
            => await Task.FromResult(_banks.Single(x => x.Name.Equals(name)).TotalSum);
    }
}