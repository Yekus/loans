﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Loans.Core.Entities;
using Loans.Core.Repositories;

namespace Loans.Infrastructure.Repositories
{
    public class InMemoryUserRepository : IUserRepository
    {
        private static ISet<User> _users = new HashSet<User>
        {
            User.Create("mattyekus@gmail.com", "admin1", Guid.NewGuid().ToString("N"), "Mateusz", "Kuchta", "1234567890", "AVT12345"),
            User.Create("mateusz.kuchta@gmail.com", "admin2", Guid.NewGuid().ToString("N"), "Mateusz", "Kuchta", "1234567890", "AVT12345"),
            User.Create("matikuchta1@gmail.com", "admin3", Guid.NewGuid().ToString("N"), "Mateusz", "Kuchta", "1234567890", "AVT12345")
        };

        public async Task<User> FindByIdAsync(Guid id)
            => await Task.FromResult(_users.SingleOrDefault(x => x.Id.Equals(id)));

        public async Task<IEnumerable<User>> GetAllAsync()
            => await Task.FromResult(_users);

        public async Task<IEnumerable<User>> GetByAsync(Func<User, bool> expression)
            => await Task.FromResult(_users.Where(expression));

        public async Task AddAsync(User entity)
        {
            _users.Add(entity);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(User entity)
        {
            var user = await FindByIdAsync(entity.Id);
            if (user != null)
            {
                _users.Remove(user);
                _users.Add(entity);               
            }
        }

        public async Task DeleteAsync(User entity)
        {
            var user = await FindByIdAsync(entity.Id);
            if (user != null)
            {
                _users.Remove(entity);
            }
            
            await Task.CompletedTask;
        }

        public async Task<User> FindByEmailAsync(string email)
            => await Task.FromResult(_users.SingleOrDefault(x => x.Email == email.ToLowerInvariant()));
    }
}