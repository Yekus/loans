﻿using System.Collections.Generic;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.DTO;

namespace Loans.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UpdateUserData, User>();
                cfg.CreateMap<Bank, BankDto>();
                cfg.CreateMap<ChangeUserPassword, User>();
            })
            .CreateMapper();
    }
}