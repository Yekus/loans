﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Infrastructure.Commands;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.Services;

namespace Loans.Infrastructure.Handlers.Users
{
    public class ChangeUserPasswordHandler : ICommandHandler<ChangeUserPassword>
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public ChangeUserPasswordHandler(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        public async Task HandleAsync(ChangeUserPassword command)
        {
            var userExists = await _userService.GetAsync(command.Email);
            if (userExists == null)
            {
                throw new Exception($"User with email {command.Email} does not exists.");
            }

            if (command.CurrentPassword == command.NewPassword)
            {
                throw new Exception($"Current and new password are the same.");
            }
            
            var newUser = _mapper.Map<ChangeUserPassword, User>(command);
            await _userService.UpdateAsync(newUser);
        }
    }
}