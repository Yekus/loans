﻿using System.Threading.Tasks;
using Loans.Infrastructure.Commands;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.Services;

namespace Loans.Infrastructure.Handlers.Users
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly IUserService _userSvc;

        public CreateUserHandler(IUserService userSvc)
        {
            _userSvc = userSvc;
        }

        public async Task HandleAsync(CreateUser command)
        {
            await _userSvc.RegisterAsync(command.Email, command.Password, command.FirstName, command.LastName,
                command.Pesel, command.DocumentNum);
        }
    }
}