﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Loans.Core.Entities;
using Loans.Infrastructure.Commands;
using Loans.Infrastructure.Commands.Users;
using Loans.Infrastructure.Services;

namespace Loans.Infrastructure.Handlers.Users
{
    public class UpdateUserDataHandler : ICommandHandler<UpdateUserData>
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UpdateUserDataHandler(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        public async Task HandleAsync(UpdateUserData command)
        {
            var userExists = await _userService.GetAsync(command.Email);
            if (userExists == null)
            {
                throw new Exception($"User with email {command.Email} does not exists.");
            }
            
            var newUser = _mapper.Map<UpdateUserData, User>(command);
            await _userService.UpdateAsync(newUser);
        }
    }
}